﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileTracker.Login
{
    interface IAppSecurity
    {
        void EncryptData(ref UserState luserState);
    }
}
