﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AgileTracker.Login
{
    class AppSecurity : IAppSecurity
    {        
        public UserState CurrentSessionUserState = new UserState();

        public AppSecurity()
        {

        }
        public void EncryptData(ref UserState luserState)
        {
            luserState.Password = EncodeData(luserState.Password);
            luserState.Username = EncodeData(luserState.Username);
            SetUserData(ref luserState);
        }

        private string EncodeData(string s)
        {

            var b = Encoding.ASCII.GetBytes(s);
            return Convert.ToBase64String(b);

        }
        private string DecodeData(string s)
        {
            var b = Convert.FromBase64String(s);
            return Encoding.ASCII.GetString(b);
        }
        private void SetUserData(ref UserState us) {
            var pwByte = Encoding.ASCII.GetBytes(us.Password);
            var pwType = SaltGen(us.Password.Length);
            us.Password = Convert.ToBase64String(GenerateSaltedHash(pwByte, pwType));
           // us.Type = pwType;
        }

        #region Crypto
        private static byte[] SaltGen(int ml)
        {
            var salt = new byte[ml * 16];
            using (var provider = new RNGCryptoServiceProvider())
            {
                provider.GetNonZeroBytes(salt);
            }
            return salt;
        }
        private static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            using (var algorithm = new MD5CryptoServiceProvider())
            {
                var saltedPT = new byte[plainText.Length + salt.Length];

                for (int i = 0; i < plainText.Length; ++i)
                {
                    saltedPT[i] = plainText[i];
                }
                for (int i = 0; i < salt.Length; ++i)
                {
                    saltedPT[plainText.Length + i] = salt[i];
                }

                return algorithm.ComputeHash(saltedPT);
            }
        }
        #endregion
    }
}
