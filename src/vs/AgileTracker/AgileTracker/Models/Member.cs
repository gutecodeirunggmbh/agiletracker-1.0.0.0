﻿using System;
using System.ComponentModel;

namespace AgileTracker
{
    [Serializable]
    public class Member : INotifyPropertyChanged
    {
        Member()
        {

        }
        public Member(string user, string pass = null, string projID = "N/A", string salt = "salt")
        {
            Username = user;
            Password = pass;
            ProjectID = projID;
            Salt = salt;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        [NonSerialized]
        private MongoDB.Bson.ObjectId _objID;
        public MongoDB.Bson.ObjectId ObjectID
        {
            get { return _objID; }
            set { _objID = value; RaisePropertyChanged("objectId"); }
        }
        private string _id;
        public string _ID
        {
            get { return ObjectID.ToString(); }
            set
            {
                _id = value;
                MongoDB.Bson.ObjectId.TryParse(value, out MongoDB.Bson.ObjectId temp);
                ObjectID = temp;
            }
        }
        private string _username;
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged("username");
            }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged("password");
            }
        }
        private string _salt;
        public string Salt
        {
            get
            {
                return _salt;
            }
            set
            {
                if (value == null || _salt == null)
                {
                    _salt = "salt";
                }
                else if (value == _salt)
                {
                    _salt = value;
                }
                else
                {
                    throw new InvalidOperationException("The salt for users is set by the server.");
                }
            }
        }
        private string _projectid;
        public string ProjectID
        {
            get { return _projectid; }
            set
            {   
                // this check is necessary to prevent an error being thrown on initialization.
                if(value == null)
                {
                    _projectid = "Uninitialized error prevention";
                }
                else if (value.Length > 0 && value != null)
                {
                    _projectid = value;
                    RaisePropertyChanged("ProjectID");
                }
                else throw new Exception("ProjectID cannot be empty.");
            }
        }
        public override string ToString()
        {
            return $"[{ObjectID}]- {Username}";
        }
    }
}
