﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AgileTracker.Models {
    [Serializable]
    public class Project : INotifyPropertyChanged {        
        public Project(string projName, string username, string userID) {            
            ProjectName = projName;
            Username = username;
            UserID = userID;
            Sprints = new List<Sprint>();
        }

        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged(string prop) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        [NonSerialized]
        private MongoDB.Bson.ObjectId _objID;
        public MongoDB.Bson.ObjectId ObjectID {
            get { return _objID; }
            set { _objID = value; RaisePropertyChanged("objectId"); }
        }

        //Class Members
        private string _projectName;
        public string ProjectName {
            get { return _projectName; }
            set {
                // this check is necessary to prevent an error being thrown on initialization.
                if (value == null)
                {
                    _projectName = "Uninitialized error prevention";
                }
                else if (value.Length > 0 && value != null) {
                    _projectName = value;
                    RaisePropertyChanged("ProjectName");
                } else throw new Exception("Project name cannot be empty.");
            }
        }
        private string _ID;
        public string _id
        {
            get { return ObjectID.ToString(); }
            set
            {
                if(value == null)
                {
                    _ID = "Uninitialized error prevention.";
                } 
                else if(value.Length > 0 && value != null)
                {
                    _ID = value;
                    MongoDB.Bson.ObjectId.TryParse(value, out MongoDB.Bson.ObjectId temp);
                    ObjectID = temp;
                    RaisePropertyChanged("_ID");
                }
            }
        }
        private string _username;
        public string Username {
            get { return _username; }
            set {
                // this check is necessary to prevent an error being thrown on initialization.
                if (value == null) {
                    _username = "Uninitialized error prevention";
                }
                else if (value.Length > 0 && value != null) {
                    _username = value;
                    RaisePropertyChanged("Username");
                } else throw new Exception("Project creator cannot be empty.");
            }
        }
        private string _userID;
        public string UserID {
            get { return _userID; }
            set {
                // this check is necessary to prevent an error being thrown on initialization.
                if (value == null) {
                    _userID = "Uninitialized error prevention";
                }
                else if (value.Length > 0 && value != null) {
                    _userID = value;
                    RaisePropertyChanged("UserID");
                } else throw new Exception("Creator UserID cannot be empty.");
            }
        }
        private List<Sprint> _sprints;
        public List<Sprint> Sprints {
            get { return _sprints; }
            set { _sprints = value; RaisePropertyChanged("Sprints"); }
        }
    }
}
