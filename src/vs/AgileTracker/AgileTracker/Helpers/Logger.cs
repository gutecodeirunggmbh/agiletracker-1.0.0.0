﻿/* Logger.cs
 * Programmer: Ryker Munn
 * Date: Feb. 16th, 2019
 * Description: Provides error logging for the Serializer.dll
 *              By default, the log is written to the C:\ drive temp directory.
 */
using System;
using System.IO;
using System.Text;

namespace AgileTracker {
    public class Logger {
        private readonly string oLogPath;
        private readonly string pFileName = "\\agtr-log.log";
        private static StringBuilder oStringBuilder;
        /// <summary>
        /// Default constructor for the Logger. Writes the log by default to the temp directory for the system.
        /// </summary>
        public Logger() {
            oLogPath = Path.GetTempPath();
            oLogPath += pFileName;
            if (oStringBuilder == null)
                oStringBuilder = new StringBuilder();
        }
        /// <summary>
        /// Recommended
        /// </summary>
        /// <param name="path">Path to write log file to</param>
        public Logger(string path) {
            oLogPath = path + pFileName;
            if (oStringBuilder == null)
                oStringBuilder = new StringBuilder();
        }
        /// <summary>
        /// Customized Logger
        /// </summary>
        /// <param name="path">Custom path to write log file to. Do not include any trailing blackslashes</param>
        /// <param name="fName">Name for the logged file. Include the file extension.</param>
        public Logger(string path, string fName) {
            pFileName = fName;
            oLogPath = path + "\\" + pFileName;
            if (oStringBuilder == null)
                oStringBuilder = new StringBuilder();
        }
        /// <summary>
        /// Writes any generic exception to the Log file
        /// </summary>
        /// <param name="ex"></param>
        public void WriteErrorLog(Exception ex) {
            oStringBuilder.AppendLine();
            oStringBuilder.Append(DateTime.Now.ToString());
            oStringBuilder.AppendFormat("\t");
            oStringBuilder.Append("Exception: ");
            oStringBuilder.Append(ex.Message);
            oStringBuilder.AppendFormat("\t");
            oStringBuilder.AppendLine();
            oStringBuilder.Append("System normal.");
            using (StreamWriter sw = new StreamWriter(oLogPath, true)) {
                sw.WriteLineAsync(oStringBuilder.ToString());
                oStringBuilder.Clear();
            }
        }
    }
}