﻿using AgileTracker.Login;
using AgileTracker.Models;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileTracker.Helpers
{
    public class NetworkRequest
    {
        public NetworkTools.API_REQUEST_TYPE eRequestType;
        public object oRequestObjectType;
        public string sOptionalMemberId;
        public NetworkRequest(NetworkTools.API_REQUEST_TYPE request, object oRequestObject, string optionalId = "optionalId")
        {
            eRequestType = request;
            oRequestObjectType = oRequestObject;
            sOptionalMemberId = optionalId;
        }
    }
    public class NetworkTools
    {
        internal static readonly HttpClient oHttpClient;
        public enum API_REQUEST_TYPE
        {
            GET = 256,
            POST = 512,
            PUT = 1024,
            DELETE = 2048
        }
        static NetworkTools()
        {
            if (oHttpClient == null)
                oHttpClient = new HttpClient();
            oHttpClient.BaseAddress = //new Uri("http://192.168.0.120:5000/agiletracker/");
    new Uri("http://ec2-35-182-26-220.ca-central-1.compute.amazonaws.com:5000/agiletracker/");
            oHttpClient.DefaultRequestHeaders.Accept.Clear();
            oHttpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        /// <summary>
        /// Performs a request against the server API using a list of parameters. 
        /// Note: for login and register requests use the public methods ExecuteLogin() and ExecuteRegister().
        /// </summary>
        /// <param name="requestOptions">An object of type NetworkRequest which provides the common interface to access the various post and get requests. </param>
        /// <returns>An awaitable task representing the results of the API request.</returns>
        public async Task<object> RequestApi(NetworkRequest requestOptions)
        {
            switch (requestOptions.oRequestObjectType.GetType().ToString())
            {
                case "AgileTracker.Models.Member":
                    {
                        Member oMember = null;
                        switch (requestOptions.eRequestType)
                        {
                            case API_REQUEST_TYPE.GET:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members GET requests.");
                                    oMember = await GetMemberFromDatabaseAsync(requestOptions.sOptionalMemberId);
                                    break;
                                }
                            case API_REQUEST_TYPE.POST:
                                {
                                    throw new NotImplementedException("This method was not initially required and as such has not been " +
                                        "implemented.");
                                }
                            case API_REQUEST_TYPE.PUT:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members PUT requests.");
                                    oMember = await UpdateMemberAsync((Member)requestOptions.oRequestObjectType);
                                    break;
                                }
                            case API_REQUEST_TYPE.DELETE:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members DELETE requests.");
                                    return await DeleteMemberAsync(requestOptions.sOptionalMemberId);
                                }
                            default:
                                throw new ArgumentException("Invalid arguments provided in NetworkRequest.eRequestType for RequestApi()");
                        }
                        return oMember;
                    }
                case "AgileTracker.Models.Project":
                    {
                        Project oProject = null;
                        switch (requestOptions.eRequestType)
                        {
                            case API_REQUEST_TYPE.GET:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members GET requests.");
                                    oProject = await GetProjectFromDatabase(requestOptions.sOptionalMemberId);
                                    break;
                                }
                            case API_REQUEST_TYPE.POST:
                                {
                                    return await AddProjectToDatabase((Project)requestOptions.oRequestObjectType);
                                }
                            case API_REQUEST_TYPE.PUT:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members PUT requests.");
                                    oProject = await UpdateProjectAsync((Project)requestOptions.oRequestObjectType);
                                    break;
                                }
                            case API_REQUEST_TYPE.DELETE:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for members DELETE requests.");
                                    return await DeleteProjectAsync(requestOptions.sOptionalMemberId);
                                }
                            default:
                                throw new ArgumentException("Invalid arguments provided in NetworkRequest.eRequestType for RequestApi()");
                        }
                        return oProject;
                    }
                case "System.Collections.Generic.List`1[AgileTracker.Models.Project]":
                    {
                        List<Project> oProjects = null;
                        switch (requestOptions.eRequestType)
                        {
                            case API_REQUEST_TYPE.GET:
                                {
                                    if (requestOptions.sOptionalMemberId == "optionalId")
                                        throw new ArgumentException("OptionalId cannot be empty for project GET requests.");
                                    oProjects = await GetProjectsFromDatabaseAsync(requestOptions.sOptionalMemberId);
                                    break;
                                }
                            default:
                                throw new InvalidOperationException("Only GET requests are supported for retrieving all projects.");
                        }
                        return oProjects;
                    }
                default:
                    throw new ArgumentException("Invalid ObjectType was passed to RequestApi().");
            }
        }
        public async Task<Member> ExecuteLogin(UserState userState)
        {
            Member oMember = null;
            oMember = await PerformLogin(userState);
            return oMember;
        }
        public void ExecuteRegister(UserState userState)
        {
            RegisterUser(userState);
        }
        #region Login_API_Requests
        private static async Task<Member> PerformLogin(UserState userState)
        {

            string[] aUserPass = { userState.Username, userState.Password };
            HttpResponseMessage response = await oHttpClient.GetAsync($"api/login/{aUserPass[0]}/{aUserPass[1]}");
            if (!response.IsSuccessStatusCode)
                throw new Exception("Login authentication failed.");
            var json = await response.Content.ReadAsStringAsync();
            var test = JsonConvert.DeserializeXmlNode(json, "Member");
            var oMember = JsonConvert.DeserializeObject<Member>(json);
            return oMember;
        }
        private static async void RegisterUser(UserState oUserState)
        {
            HttpResponseMessage response = await oHttpClient.PostAsJsonAsync("api/login", oUserState);
            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new Exception(response.ReasonPhrase);
            }
        }
        #endregion  
        #region Member_API_Requests
        private static async Task<Uri> AddMemberToDatabaseAsync(Member member)
        {
            HttpResponseMessage response = await oHttpClient.PostAsJsonAsync("api/members", member);
            response.EnsureSuccessStatusCode();

            // return the URI 
            return response.Headers.Location;
        }
        private static async Task<Member> GetMemberFromDatabaseAsync(string sUserId)
        {
            try
            {
                Member oMember = null;
                HttpResponseMessage response = await oHttpClient.GetAsync($"api/members/{sUserId}");
                if (response.IsSuccessStatusCode)
                {
                    oMember = JsonConvert.DeserializeObject<Member>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new HttpListenerException((int)response.StatusCode, response.ReasonPhrase);
                }
                return oMember;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static async Task<Member> UpdateMemberAsync(Member member)
        {
            HttpResponseMessage response = await oHttpClient.PutAsJsonAsync($"/members/{member.Username}", member);
            response.EnsureSuccessStatusCode();

            // deserialize the response body and return the new member
            member = JsonConvert.DeserializeObject<Member>(await response.Content.ReadAsStringAsync());
            return member;
        }
        private static async Task<HttpStatusCode> DeleteMemberAsync(string memberId)
        {
            HttpResponseMessage response = await oHttpClient.DeleteAsync(
                $"api/members/{memberId}");
            return response.StatusCode;
        }
        #endregion
        #region Project_API_Requests
        private static async Task<Uri> AddProjectToDatabase(Project project)
        {
            HttpResponseMessage response = await oHttpClient.PostAsJsonAsync("api/project", project);
            response.EnsureSuccessStatusCode();

            // return the URI 
            return response.Headers.Location;
        }
        private static async Task<Project> GetProjectFromDatabase(string sUserId)
        {
            try
            {
                Project oProject = null;
                HttpResponseMessage response = await oHttpClient.GetAsync($"api/projects/{sUserId}");
                if (response.IsSuccessStatusCode)
                {
                    oProject = JsonConvert.DeserializeObject<Project>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new HttpListenerException((int)response.StatusCode, response.ReasonPhrase);
                }
                return oProject;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static async Task<Project> UpdateProjectAsync(Project project)
        {
            HttpResponseMessage response = await oHttpClient.PutAsJsonAsync($"api/project", project);
            response.EnsureSuccessStatusCode();

            // deserialize the response body and return the new member
            project = JsonConvert.DeserializeObject<Project>(await response.Content.ReadAsStringAsync());
            return project;
        }
        private static async Task<HttpStatusCode> DeleteProjectAsync(string projectId)
        {
            HttpResponseMessage response = await oHttpClient.DeleteAsync(
                $"api/projects/{projectId}");
            return response.StatusCode;
        }
        private static async Task<List<Project>> GetProjectsFromDatabaseAsync(string sProjectId)
        {
            List<Project> oProjects = null;
            HttpResponseMessage response = await oHttpClient.GetAsync($"api/projects/{sProjectId}");
            if (response.IsSuccessStatusCode)
            {
                 oProjects = await response.Content.ReadAsAsync<List<Project>>();
               // oProjects = JsonConvert.DeserializeObject<Project[]>(test);
            }
            else
                throw new HttpListenerException((int)response.StatusCode, response.ReasonPhrase);

            return oProjects;
        }
        #endregion


    }
}
