﻿using AgileTracker.Helpers;
using AgileTracker.Login;
using AgileTracker.Models;
using AgileTracker.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace AgileTracker.Controller
{
    public class ViewModelMain : DependencyObject
    {
        #region PropertyChanged Event Handler
        public event PropertyChangedEventHandler PropertyChanged;
        void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        #endregion

        //Login Member Object
        public Member currLoggedIn = null;

        //Constructor
        public ViewModelMain()
        {
            //Initialize the logger
            oLogger = new Logger(Directory.GetCurrentDirectory());
            oNetworkTools = new NetworkTools();

            ProjDGCollection = new ObservableCollection<Project>(); //{ new Project("ye","no","1"), new Project("ye", "no", "1") };
            SprintDGCollection = new ObservableCollection<Sprint>(); //{ new Sprint(1), new Sprint(2) };
            TaskDGCollection = new ObservableCollection<Task>();
            MemberDGCollection = new ObservableCollection<Member> { new Member("") };

            //Initialize the relaycommands
            OpenAllProjectsWindowCommand = new RelayCommand(OpenAllProjectsWindowImp);
            OpenAddNewProjectWindowCommand = new RelayCommand(OpenAddNewProjectWindowImp);
            AddNewProjectCommand = new RelayCommand(AddNewProjectImp);
            RemoveSelectedProjectCommand = new RelayCommand(RemoveSelectedProjectImp);
            ClearNewProjectNameCommand = new RelayCommand(ClearNewProjectNameImp);
            OpenViewSelectedProjectWindowCommand = new RelayCommand(OpenViewSelectedProjectWindowImp);
            AddNewSprintCommand = new RelayCommand(AddNewSprintImp);
            RemoveSelectedSprintCommand = new RelayCommand(RemoveSelectedSprintImp);
            OpenViewSelectedSprintWindowCommand = new RelayCommand(OpenViewSelectedSprintWindowImp);
            OpenAddNewTaskWindowCommand = new RelayCommand(OpenAddNewTaskWindowImp);
            AddNewTaskCommand = new RelayCommand(AddNewTaskImp);
            RemoveSelectedTaskCommand = new RelayCommand(RemoveSelectedTaskImp);
            OpenViewSelectedTaskWindowCommand = new RelayCommand(OpenViewSelectedTaskWindowImp);
            OpenLoginWindowCommand = new RelayCommand(OpenLoginWindowImp);
            LoginCommand = new RelayCommand(LoginImp);
            RegisterCommand = new RelayCommand(RegisterImp);
            LoadAllProjectsCommand = new RelayCommand(LoadAllProjectsImp);
            SaveAllProjectsCommand = new RelayCommand(SaveAllProjectsImp);
        }

        #region Helper Declaration
        internal readonly NetworkTools oNetworkTools;
        internal static Logger oLogger;
        #endregion

        #region Window Declarations
        ViewProjectsWindow viewProjectsWindow = null;
        AddNewProjectWindow addNewProjectWindow = null;
        ViewSelectedProjectWindow viewSelectedProjectWindow = null;
        ViewSelectedSprintWindow viewSelectedSprintWindow = null;
        AddTaskWindow addTaskWindow = null;
        ViewSelectedTaskWindow viewSelectedTaskWindow = null;
        LoginWindow loginWindow = null;
        #endregion

        #region Observable Collections
        //Observable Collections
        //--ViewProjectsWindow DataGrid Collections        
        public ObservableCollection<Project> ProjDGCollection
        {
            get { return (ObservableCollection<Project>)GetValue(projDGCollectionProperty); }
            set { SetValue(projDGCollectionProperty, value); }
        }
        public static readonly DependencyProperty projDGCollectionProperty =
            DependencyProperty.Register("projDGCollection", typeof(ObservableCollection<Project>),
                typeof(ViewModelMain), new PropertyMetadata(null));

        public ObservableCollection<Sprint> SprintDGCollection
        {
            get { return (ObservableCollection<Sprint>)GetValue(sprintDGCollectionProperty); }
            set { SetValue(sprintDGCollectionProperty, value); }
        }
        public static readonly DependencyProperty sprintDGCollectionProperty =
            DependencyProperty.Register("sprintDGCollection", typeof(ObservableCollection<Sprint>),
                typeof(ViewModelMain), new PropertyMetadata(null));
        public ObservableCollection<Task> TaskDGCollection
        {
            get { return (ObservableCollection<Task>)GetValue(TaskDGCollectionProperty); }
            set { SetValue(TaskDGCollectionProperty, value); }
        }
        public static readonly DependencyProperty TaskDGCollectionProperty =
            DependencyProperty.Register("taskDGCollection", typeof(ObservableCollection<Task>), typeof(ViewModelMain), new PropertyMetadata(null));
        public ObservableCollection<Member> MemberDGCollection
        {
            get { return (ObservableCollection<Member>)GetValue(MemberDGCollectionProperty); }
            set { SetValue(MemberDGCollectionProperty, value); }
        }
        public static readonly DependencyProperty MemberDGCollectionProperty =
            DependencyProperty.Register("memberDGCollection", typeof(ObservableCollection<Member>), typeof(ViewModelMain), new PropertyMetadata(null));



        public Project SelectedProject
        {
            get { return (Project)GetValue(SelectedProjectProperty); }
            set { SetValue(SelectedProjectProperty, value); }
        }
        public static readonly DependencyProperty SelectedProjectProperty =
            DependencyProperty.Register("SelectedProject", typeof(Project), typeof(ViewModelMain), new PropertyMetadata(null));
        public Sprint SelectedSprint
        {
            get { return (Sprint)GetValue(SelectedSprintProperty); }
            set { SetValue(SelectedSprintProperty, value); }
        }
        public static readonly DependencyProperty SelectedSprintProperty =
            DependencyProperty.Register("SelectedSprint", typeof(Sprint), typeof(ViewModelMain), new PropertyMetadata(null));
        public Task SelectedTask
        {
            get { return (Task)GetValue(SelectedTaskProperty); }
            set { SetValue(SelectedTaskProperty, value); }
        }
        public static readonly DependencyProperty SelectedTaskProperty =
            DependencyProperty.Register("SelectedTask", typeof(Task), typeof(ViewModelMain), new PropertyMetadata(null));

        #endregion

        #region DependencyProperty Variables
        private string _newProjectName;
        public string NewProjectName { get { return _newProjectName; } set { _newProjectName = value; RaisePropertyChanged("NewProjectName"); } }
        private string _newSprintName;
        public string NewSprintName { get { return _newSprintName; } set { _newSprintName = value; RaisePropertyChanged("NewSprintName"); } }

        private string _loginName;
        public string LoginName { get { return _loginName; } set { _loginName = value; RaisePropertyChanged("LoginName"); } }

        private bool _enableProjects = true;
        public bool EnableProjects { get { return _enableProjects; } set { _enableProjects = value; RaisePropertyChanged("EnableProjects"); } }
        #endregion

        #region RelayCommands Declarations
        public RelayCommand OpenAllProjectsWindowCommand { get; set; }
        public RelayCommand OpenAddNewProjectWindowCommand { get; set; }
        public RelayCommand AddNewProjectCommand { get; set; }
        public RelayCommand RemoveSelectedProjectCommand { get; set; }
        public RelayCommand ClearNewProjectNameCommand { get; set; }
        public RelayCommand OpenViewSelectedProjectWindowCommand { get; set; }
        public RelayCommand AddNewSprintCommand { get; set; }
        public RelayCommand RemoveSelectedSprintCommand { get; set; }
        public RelayCommand OpenViewSelectedSprintWindowCommand { get; set; }
        public RelayCommand OpenAddNewTaskWindowCommand { get; set; }
        public RelayCommand AddNewTaskCommand { get; set; }
        public RelayCommand RemoveSelectedTaskCommand { get; set; }
        public RelayCommand OpenViewSelectedTaskWindowCommand { get; set; }
        public RelayCommand OpenLoginWindowCommand { get; set; }
        public RelayCommand LoginCommand { get; set; }
        public RelayCommand RegisterCommand { get; set; }
        public RelayCommand LoadAllProjectsCommand { get; set; }
        public RelayCommand SaveAllProjectsCommand { get; set; }

        #endregion

        #region RelayCommands Implementations
        private void OpenAllProjectsWindowImp(object obj)
        {
            try
            {
                viewProjectsWindow = new ViewProjectsWindow();
                viewProjectsWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenAddNewProjectWindowImp(object obj)
        {
            try
            {
                addNewProjectWindow = new AddNewProjectWindow();
                addNewProjectWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void AddNewProjectImp(object obj)
        {
            try
            {
                if (NewProjectName.Equals(""))
                    throw new Exception("A project name is required.");
                foreach (Project proj in ProjDGCollection)
                {
                    if (proj.ProjectName.Equals(NewProjectName))
                        throw new Exception("A project already exists with that name.");
                }
                Project newProject = new Project(NewProjectName, currLoggedIn.Username, currLoggedIn.ObjectID.ToString());
                NetworkRequest networkRequest = new NetworkRequest(NetworkTools.API_REQUEST_TYPE.POST, newProject);
                var uri = await oNetworkTools.RequestApi(networkRequest); // creating a new project will return the URI that the Project was posted to.
                ProjDGCollection.Add(newProject);

                addNewProjectWindow.Close();
                MessageBox.Show("Project successfully created!", "Project Created", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void RemoveSelectedProjectImp(object obj)
        {
            try
            {
                NetworkRequest networkRequest = new NetworkRequest(NetworkTools.API_REQUEST_TYPE.DELETE, SelectedProject, SelectedProject.ObjectID.ToString());
                var statusCode = (System.Net.Http.HttpResponseMessage)(await oNetworkTools.RequestApi(networkRequest)); // creating a new project will return the URI that the Project was posted to.
                if (statusCode.IsSuccessStatusCode)
                    ProjDGCollection.Remove(SelectedProject);
                else
                    throw new System.Net.HttpListenerException((int)statusCode.StatusCode, statusCode.ReasonPhrase);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearNewProjectNameImp(object obj)
        {
            try
            {
                NewProjectName = "";
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenViewSelectedProjectWindowImp(object obj)
        {
            try
            {
                viewSelectedProjectWindow = new ViewSelectedProjectWindow();
                SprintDGCollection.Clear();
                SprintDGCollection = new ObservableCollection<Sprint>(SelectedProject.Sprints);
                viewSelectedProjectWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddNewSprintImp(object obj)
        {
            try
            {
                Sprint newSprint = new Sprint(SelectedProject.Sprints.Count() + 1);
                SelectedProject.Sprints.Add(newSprint);
                SprintDGCollection.Add(newSprint);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RemoveSelectedSprintImp(object obj)
        {
            try
            {
                SelectedProject.Sprints.Remove(SelectedSprint);
                SprintDGCollection.Remove(SelectedSprint);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenViewSelectedSprintWindowImp(object obj)
        {
            try
            {
                viewSelectedSprintWindow = new ViewSelectedSprintWindow();
                TaskDGCollection = new ObservableCollection<Task>(SelectedSprint.TaskList);
                viewSelectedSprintWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenAddNewTaskWindowImp(object obj)
        {
            try
            {
                addTaskWindow = new AddTaskWindow();
                addTaskWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddNewTaskImp(object obj)
        {
            try
            {
                string tID = (SelectedSprint.TaskList.Count() + 1).ToString();
                string UserID = currLoggedIn.ObjectID.ToString();
                string Username = currLoggedIn.Username;
                List<Member> Members = new List<Member>(MemberDGCollection);
                Task.TaskDescriptors State;
                switch (addTaskWindow.cb_status.Text)
                {
                    case "Not_Started":
                        State = Task.TaskDescriptors.Not_Started;
                        break;
                    case "In_Progress":
                        State = Task.TaskDescriptors.In_Progress;
                        break;
                    case "Finished":
                        State = Task.TaskDescriptors.Finished;
                        break;
                    default:
                        State = Task.TaskDescriptors.Not_Started;
                        break;
                }
                double EstimatedTime = double.Parse(addTaskWindow.tb_estimatedTime.Text);
                double ActualTime = double.Parse(addTaskWindow.tb_actualTime.Text);
                string Details = addTaskWindow.tb_details.Text;
                Task newTask = new Task(tID, UserID, Username, Members, State, EstimatedTime, ActualTime, Details);
                SelectedSprint.TaskList.Add(newTask);
                TaskDGCollection.Add(newTask);
                //Trigger property changed to attempt to update sprint count number
                RaisePropertyChanged("sprintDGCollection");
                addTaskWindow.Close();
                MessageBox.Show("Task successfully added.", "Task Added");
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RemoveSelectedTaskImp(object obj)
        {
            try
            {
                SelectedSprint.TaskList.Remove(SelectedTask);
                TaskDGCollection.Remove(SelectedTask);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenViewSelectedTaskWindowImp(object obj)
        {
            try
            {
                viewSelectedTaskWindow = new ViewSelectedTaskWindow();
                MemberDGCollection = new ObservableCollection<Member>(SelectedTask.Members);
                viewSelectedTaskWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenLoginWindowImp(object obj)
        {
            try
            {
                loginWindow = new LoginWindow();
                loginWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RegisterImp(object obj)
        {
            try
            {
                //Create user state to pass to encrypt
                UserState state = new UserState
                {
                    Username = loginWindow.tb_username.Text,
                    Password = loginWindow.tb_password.Password
                };
                //Create API Request
                oNetworkTools.ExecuteRegister(state);
                MessageBox.Show("User successfully registered", "Registration", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void LoginImp(object obj)
        {
            currLoggedIn = null;
            try
            {
                //Create user state to pass to encrypt
                UserState state = new UserState
                {
                    Username = loginWindow.tb_username.Text,
                    Password = loginWindow.tb_password.Password
                };
                currLoggedIn = (Member)await oNetworkTools.ExecuteLogin(state);
                if (currLoggedIn != null)
                {
                    EnableProjects = true;
                    MessageBox.Show($"Now signed in as {state.Username}.");
                }
                else
                {
                    MessageBox.Show($"Failed to log in.");
                }

                loginWindow.Close();
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void LoadAllProjectsImp(object obj)
        {
            try
            {
                NetworkRequest apiReq = new NetworkRequest(NetworkTools.API_REQUEST_TYPE.GET, new List<Project>(), currLoggedIn.ObjectID.ToString());
                List<Project> returnedProjects = (List<Project>)await oNetworkTools.RequestApi(apiReq);
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void SaveAllProjectsImp(object obj)
        {
            try
            {
                foreach (Project proj in ProjDGCollection)
                {
                    NetworkRequest apiReq = new NetworkRequest(NetworkTools.API_REQUEST_TYPE.PUT, proj, proj.ObjectID.ToString());
                    await oNetworkTools.RequestApi(apiReq);
                }
            }
            catch (Exception ex)
            {
                oLogger.WriteErrorLog(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
