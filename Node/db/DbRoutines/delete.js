let ObjectID = require("mongodb").ObjectID;

const deleteOne = (proj, db, coll) => {
  let realId = new ObjectID(proj._id);
  return db.collection(coll).deleteOne({ _id: realId });
};

module.exports = { deleteOne };
