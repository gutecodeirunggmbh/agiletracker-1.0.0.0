/*Get data from the mongo DB will return in JSON format*/
const findByUserNamePassword = (user, db) => {
  return db.collection("users").findOne(user);
};

// Just find by username
const findByUserName = (userName, db) => {
  return db.collection("users").findOne({ Username: userName });
};

// Just find by username
const findById = (id, db, coll) => {
  let realId = new ObjectID(id);
  return db.collection(coll).findOne({ _id: realId });
};

// Task finds
// get all tasks for a userName
const findTasksByUserName = (userId, db) => {
  return db
    .collection("tasks")
    .find({ userid: userId })
    .toArray();
};

// get all projects for a username
const findByUserId = (userNameID, db, coll) => {
  return db
    .collection(coll)
    .find({
      _userID: userNameID
    })
    .toArray();
};

// end task finds
module.exports = {
  findByUserNamePassword,
  findByUserName,
  findTasksByUserName,
  findByUserId,
  findById
};
