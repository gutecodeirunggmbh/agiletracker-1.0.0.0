let ObjectID = require("mongodb").ObjectID;

// update a user
const updateOneUser = (user, db, coll) => {
  let realId = new ObjectID(user._id);
  return db
    .collection(coll)
    .updateOne({ _id: realId }, { $set: { ProjectId: user.ProjectId } });
};

const updateOneProjectTasks = (project, db, coll) => {
  let realId = new ObjectID(project._id);
  return db.collection("project").replaceOne({ _id: realId }, { project });
};

module.exports = { updateOneProjectTasks, updateOneUser };
