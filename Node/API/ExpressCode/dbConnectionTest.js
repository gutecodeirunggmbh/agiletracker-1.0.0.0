var mongoClient = require("mongodb").MongoClient;
const userName = `twoods`;
const password = `W+ufu4A68bzWOsj9peLc+I4rI2cg5KtLP+9haT1TrevEw8r4TYExrcH8j35uyQhB`;
const connUrl = `mongodb://${userName}:${password}@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017/test`;
const dataRtns = require("../../db/DbRoutines/inserts");
const connectTest = async () => {
  let conn = undefined;
  try {
    conn = await mongoClient.connect(`${connUrl}`, {
      useNewUrlParser: true
    });
    console.log("Connected");
    // connect to the DB

    const db = await conn.db(`test`);

    let testInsert = { name: "tommy" };
    let x = await dataRtns.addOneToCollection(testInsert, db, `insert`);
    console.log("Inserted Count: " + x.insertedCount);
  } catch (err) {
    console.log(err);
  } finally {
    conn ? conn.close : null;
    console.log(`Disconnected from the devtest`);
  }
  process.exit(0);
};

const addTestUser = async () => {
  let conn = undefined;
  try {
    conn = await mongoClient.connect(`${connUrl}`, {
      authMechanism: `SCRAM-SHA-1`,
      auth: {
        userName: `${userName}`,
        password: `${password}`
      },
      useNewUrlParser: true
    });
    // connect to the DB
    const db = conn.db(`${process.env.DBURL}`);

    // create a test user to insert into the user collection
    let user = {
      username: "testFind",
      password: "pass"
    };
    // insert into the usersCollection just 1 user
    let inserted = await dataRtns.addOneToCollection(user, db, "insert");
    // see if it worked
    console.log(inserted.insertedCount);

    // insert many test uncomment to run
    //// inserted = await insertRtns.addMany(json, db, "twoods_dev");

    // console.log(inserted.insertedCount);
  } catch (err) {
    console.log(err);
  } finally {
    conn ? conn.close : null;
    console.log(`Disconnected from the ${process.env.DBURL}`);
  }
  process.exit(0);
};

connectTest();
