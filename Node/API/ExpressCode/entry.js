//const env = require("dotenv").config({ path: `$../../../common/config/.env` });
const logger = require("./logger");
const apiMasterRoutes = require("./apiMasterRoutes");
const apiPostRoutes = require("./postRoutes");
const apiGetRoutes = require("./getRoutes");
const apiDelRoutes = require("./deleteRoute");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
var http = require("http");
const port = 5000;
// cors - remove in prod
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
  next();
});

// parse application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use((req, res, next) => {
//   logger.WriteConnectionLog(process.env.CONNLOG);
//   next();
// });

// app.use("/agiletracker/api", apiMasterRoutes);

// app.listen(port, `192.168.0.111`, () => {
// console.log(`listening on port ${port}`);
// });

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
app.use(express.static("public"));

app.use("/agiletracker/api", apiPostRoutes);
app.use("/agiletracker/api", apiGetRoutes);
app.use("/agiletracker/api", apiDelRoutes);
