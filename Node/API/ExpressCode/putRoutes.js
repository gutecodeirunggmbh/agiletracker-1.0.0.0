const env = require("dotenv").config({ path: `${__dirname}/.env` });
const express = require("express");
const router = express.Router();
const mongoClient = require("mongodb").MongoClient;
const dataRtns = require("../../db/DbRoutines/updates");

// const connUrl = `mongodb://${process.env.USERNAME}:${
//   process.env.PASSWORD
// }@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017:/test`;

const userName = `twoods`;
const password = `W+ufu4A68bzWOsj9peLc+I4rI2cg5KtLP+9haT1TrevEw8r4TYExrcH8j35uyQhB`;
const connUrl = `mongodb://${userName}:${password}@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017/test`;
router.put("/project", async (req, res) => {
  let conn;
  try {
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    const db = conn.db(process.env.DB);
    let x = await dataRtns.updateOneProjectTasks(
      req.body,
      db,
      process.env.PROJECTSCOLLECTION
    );
    res.send(x.updatedCount);
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("get all users failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});
