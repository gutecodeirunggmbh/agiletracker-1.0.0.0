const env = require("dotenv").config({ path: `${__dirname}/.env` });
const express = require("express");
const router = express.Router();
const mongoClient = require("mongodb").MongoClient;
const dataRtns = require("../../db/DbRoutines/inserts");
const updateRtns = require("../../db/DbRoutines/updates");
const cryptoRtns = require("../ExpressCode/loginCrypto");

// const connUrl = `mongodb://${process.env.USERNAME}:${
//   process.env.PASSWORD
// }@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017:/test`;

const userName = `twoods`;
const password = `W+ufu4A68bzWOsj9peLc+I4rI2cg5KtLP+9haT1TrevEw8r4TYExrcH8j35uyQhB`;
const connUrl = `mongodb://${userName}:${password}@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017/test`;
router.post("/login", async (req, res) => {
  let conn;
  try {
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    console.log("Inside the user login add");
    const db = conn.db(process.env.DB);
    let password = req.body.Password;
    let hashedPw = cryptoRtns.HashSaltPassword(password, cryptoRtns.GenerateRanndomSalt(password.length));
    let user = {
      Username: req.body.Username,
      Password: hashedPw.passwordHash,
      salt: hashedPw.salt,
      ProjectID: 'n/a' // resolves an issue with registering new users and deserializing their login request.
    };
    let x = await dataRtns.addOneToCollection(
      user,
      db,
      process.env.USERSCOLLECTION
    );
    res.send(x);
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("get all users failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

router.post("/project", async (req, res) => {
  let conn;
  try {
    console.log("Inside add");
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    const db = conn.db(process.env.DB);
    let added = await dataRtns.addOneToCollection(
      req.body,
      db,
      process.env.PROJECTSCOLLECTION
    );
    res.send("added project");
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("add all projects failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

router.post("/task", async (req, res) => {
  let conn;
  try {
    console.log("Inside add");
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    const db = conn.db(process.env.DB);
    let added = await dataRtns.addOneToCollection(
      req.body,
      db,
      process.env.TASKSCOLLECTION
    );

    let updated = await updateRtns.updateOneProjectTasks(req.body, db);
    console.log("Updated the project with the new task");
    res.send("added task");
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("add all projects failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

module.exports = router;
