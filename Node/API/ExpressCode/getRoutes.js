const env = require("dotenv").config({ path: `${__dirname}/.env` });
const express = require("express");
const router = express.Router();
const mongoClient = require("mongodb").MongoClient;
const dataRtns = require("../../db/DbRoutines/gets");
const crytpoRtn = require("../ExpressCode/loginCrypto");

// const connUrl = `${process.env.CONNDB}://${process.env.USERNAME}:${
//   process.env.PASSWORD
// }${process.env.AWSHOST}:${process.env.PORT}/${process.env.AUTH}`;

const userName = `twoods`;
const password = `W+ufu4A68bzWOsj9peLc+I4rI2cg5KtLP+9haT1TrevEw8r4TYExrcH8j35uyQhB`;
const connUrl = `mongodb://${userName}:${password}@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017/test`;
// define a default route to get the login for a user
router.get("/login/:username/:password", async (req, res) => {
  let conn;
  let userName = req.params.username;
  let password = req.params.password;
  try {
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    console.log("inside login get");
    const db = conn.db(process.env.DB);
    console.log(userName);
    let user = undefined;
    let userFound = await dataRtns.findByUserName(userName, db);
    if (userFound) {
      console.log(`User found`);
      console.log(userFound);
      console.log(`User found SALT`);
      console.log(userFound.salt);
      let hash = crytpoRtn.HashSaltPassword(password, userFound.salt);
      user = { Username: userName, Password: hash.passwordHash, salt: hash.salt };
      let users = await dataRtns.findByUserNamePassword(
        user,
        db,
        process.env.USERCOLLECTION
      );
      console.log(users);
      if (users) {
        res.send(users);
      }
      else res.status(500).send("get all users failed - internal server error");
    }
    else {
      res.status(500).send("Username does not exist.");
    }
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("get all users failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

router.get("/tasks:/userid", async (req, res) => {
  let userId = req.params.userid;
  let conn;
  try {
    conn = await mongoClient.connect(connUrlL, {
      useNewUrlParser: true
    });
    const db = conn.db(process.env.DB);
    let users = await dataRtns.findTasksByUserName(
      userId,
      db,
      process.env.USERCOLLECTION
    );
    if (users) res.send(users);
    else res.send("user not found");
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("get all users failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

router.get("/projects/:userid", async (req, res) => {
  let userId = req.params.userid;
  let conn;
  try {
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    const db = conn.db(process.env.DB);
    let users = await dataRtns.findByUserId(
      userId,
      db,
      process.env.PROJECTSCOLLECTION
    );
    if (users) res.send(users);
    else res.status(500).send("Projects not found");
  } catch (err) {
    console.log(err.stack);
    res.status(500).send("get all users failed - internal server error");
  } finally {
    if (conn) conn.close();
  }
});

module.exports = router;
