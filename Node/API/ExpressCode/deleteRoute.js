const env = require("dotenv").config({ path: `${__dirname}/.env` });
const express = require("express");
const router = express.Router();
const mongoClient = require("mongodb").MongoClient;
const dataRtns = require("../../db/DbRoutines/gets");
const delRtns = require("../../db/DbRoutines/delete");

const userName = `twoods`;
const password = `W+ufu4A68bzWOsj9peLc+I4rI2cg5KtLP+9haT1TrevEw8r4TYExrcH8j35uyQhB`;
const connUrl = `mongodb://${userName}:${password}@ec2-35-183-243-71.ca-central-1.compute.amazonaws.com:27017/test`;

router.delete("/project/:_projectid", async (req, res) => {
  let id = req.params._projectid;
  try {
    conn = await mongoClient.connect(connUrl, {
      useNewUrlParser: true
    });
    console.log("inside delete route");
    const db = conn.db(process.env.DB);

    let project = await dataRtns.findById(
      id,
      db,
      process.env.PROJECTSCOLLECTION
    );

    console.log(`Project found = ${project}`);
    let delProj = await delRtns.deleteOne(
      project,
      db,
      process.env.PROJECTSCOLLECTION
    );

    console.log(delProj.deletedCount + "Projects deleted from the DB");
    res.send(`${delProj.deletedCount} project(s) deleted from the DB!`);
  } catch (err) {
    console.log(`Houston we have a problem: ${err}`);
  } finally {
    conn ? conn.close : null;
    process.exit(0);
  }
});

module.exports = router;
